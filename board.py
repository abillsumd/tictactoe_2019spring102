""" Implements a tic tac toe board. """

class Board:
    """
    A tic tac toe board.

    Attributes:
        game (list of Player): a list with nine items representing the spaces
            on a tic tac toe board. None values represent empty squares.
    """
    def __init__(self):
        """
        Initialize an empty board.
        """
        self.game=[None,None,None,None,None,None,None,None,None]


    def game_over(self):
        """ Return True if the game is over, otherwise False. """
        if self.get_free_squares() == []:
            return True
        elif self.get_winner() != None:
            return True
        else:
            return False


        raise NotImplementedError

    def get_free_squares(self):
        """ Return a list of integers indicating which squares are empty. """
        self.free_squares = []
        for counter, value in enumerate(self.game):
            if value == None:
                self.free_squares.append(counter)
        return self.free_squares


    def get_winner(self):
        """ Return a Player object if there is a winner, otherwise None. """
        #across top
        if self.game[0] == self.game[1] == self.game[2] and self.game[0] != None:
            winner = self.game[0]
        #across middle
        elif self.game[3] == self.game[4] == self.game[5] and self.game[3] != None:
            winner = self.game[3]
        #across bottom
        elif self.game[6] == self.game[7] == self.game[8] and self.game[6] != None:
            winner = self.game[6]
        #down left
        elif self.game[0] == self.game[3] == self.game[6] and self.game[0] != None:
            winner = self.game[0]
        #down left
        elif self.game[1] == self.game[4] == self.game[7] and self.game[1] != None:
            winner = self.game[1]
        #down right
        elif self.game[2] == self.game[5] == self.game[8] and self.game[2] != None:
            winner = self.game[2]
        #diagonal left
        elif self.game[0] == self.game[4] == self.game[8] and self.game[0] != None:
            winner = self.game[0]
        #diagonal right
        elif self.game[2] == self.game[4] == self.game[6] and self.game[2] != None:
            winner = self.game[2]
        else:
            winner = None
        return winner

    def player_pick(self, free_square, player):
        """
        Assign free_square to player.

        Args:
            free_square (int): an index into game representing the square the
                player wishes to claim.
            player (Player): the player claiming the square.

        Side effects:
            set the selected square to the Player who selected it.

        Raises:
            ValueError: the requested square is already claimed.
        """
        if free_square not in self.get_free_squares():
            raise ValueError
        self.game[free_square] = player

    def __str__(self):
        """ Return a string representation of the board. """
        rows = []
        for i in range(0, 9, 3):
            row = []
            for j in range(i, i+3):
                if self.game[j] is None:
                    row.append(str(j))
                else:
                    row.append(self.game[j].symbol)
            rows.append(' '.join(row))
        return '\n'.join(rows)
