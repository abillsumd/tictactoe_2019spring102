"""
Define unit tests for select methods from the Board class.

Slightly improved/cleaned up from the version we created in class.
"""

import unittest
from board import Board
from player import HumanPlayer, ComputerPlayer


class TestBoard(unittest.TestCase):
    """
    Unit tests for the Board component of our tic tac toe game.
    """
    def setUp(self):
        """
        Create some objects that we can use for each test.
        These will be re-created for each test we run.
        """
        self.board = Board()
        self.p1 = ComputerPlayer('Steve')
        self.p2 = ComputerPlayer('Charlotte')


    def test_init(self):
        """
        Make sure Board.__init__() creates an empty board in the format
        we expect.
        """
        # A new board should have a game attribute which is a list with
        # nine None values.
        self.assertEqual(self.board.game, [None]*9,
                         msg="game attribute has unexpected value")

    def test_game_over(self):
        """
        Make sure Board.game_over() correctly detects whether the game
        is over under several different scenarios.
        """
        # When the board is empty, the game should not be over.
        self.assertFalse(self.board.game_over(),
                         msg="game_over() returns wrong result for an empty"
                         " board")
        # If a player has won the game, game_over() should return True
        # even if there are free squares in the board.
        self.board.game = [self.p1, self.p1, self.p1,
                           None,    self.p2, None,
                           self.p2, None,    self.p2]
        self.assertTrue(self.board.game_over(),
                        msg="game_over() returns wrong result if a player wins"
                        " before all squares are claimed")
        self.board.game = [self.p1, self.p2, self.p1,
                           self.p2, self.p2, self.p1,
                           self.p1, self.p1, self.p2]
        self.assertTrue(self.board.game_over(),
                        msg="game_over() returns wrong result if the game ends"
                        " in a draw")

    def test_free_squares(self):
        """
        Ensure that Board.get_free_squares() returns a correct result
        for a number of scenarios.
        """
        # At the beginning of the game, all 8 squares should be free.
        expected = {0, 1, 2, 3, 4, 5, 6, 7, 8}
        self.assertEqual(expected, set(self.board.get_free_squares()),
                         msg="get_free_sqares() returns wrong result for an"
                         " empty board")
        # For the following board, only squares 3, 5, and 7 should be free.
        self.board.game = [self.p1, self.p1, self.p1,
                           None,    self.p2, None,
                           self.p2, None,    self.p2]
        expected = {3, 5, 7}
        self.assertEqual(expected, set(self.board.get_free_squares()),
                         msg="get_free_squares() returns wrong result if there"
                         " are free squares but there is a winner")
        self.board.game = [self.p1, self.p2, self.p1,
                           self.p2, self.p2, self.p1,
                           self.p1, self.p1, self.p2]
        # If all squares are claimed, there should be no free squares.
        expected = set()
        self.assertEqual(expected, set(self.board.get_free_squares()),
                         msg="get_free_squares() returns wrong result if the"
                         " game ends in a draw")

    def test_player_pick(self):
        """
        Ensure that Board.player_pick() alters the board as expected.
        """
        self.board.player_pick(3, self.p2)
        # make sure the requested square was assigned to the player
        self.assertEqual(self.board.game[3], self.p2,
                         msg="player_pick() does not assign the requested"
                         " square to the requesting player")
        # make sure the square was only assigned once
        self.assertEqual(self.board.game.count(self.p2), 1,
                         msg="player_pick() assigns the wrong number of squares"
                         " to the requesting player")
        # make sure an error is raised if another user tries to claim an
        # already-claimed square
        # note that for this test we are using assertRaises as a context manager
        with self.assertRaises(ValueError, msg="player_pick() fails to raise"
                               " ValueError if a player requests an already"
                               "-claimed square"):
            self.board.player_pick(3, self.p1)

    def test_str(self):
        """
        Ensure that Board.__str__() returns the expected representation
        of the board.
        """
        # check the representation of an empty board
        self.assertIsInstance(self.board.__str__(), str)
        s = "0 1 2\n3 4 5\n6 7 8"
        self.assertEqual(str(self.board), s, msg="__str__() returns wrong value"
                         " when board is blank")
        # check the representation of a board with squares claimed
        self.p1.symbol='o'
        self.p2.symbol='x'
        self.board.game = [self.p1, self.p1, self.p1,
                           None,    self.p2, None,
                           self.p2, None,    self.p2]
        s = "o o o\n3 x 5\nx 7 x"
        self.assertEqual(str(self.board), s, msg="__str__() returns wrong value"
                         " when board has some claimed spaces")

    # more tests would go here


if __name__ == '__main__':
    unittest.main()
