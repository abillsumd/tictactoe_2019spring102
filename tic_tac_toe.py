""" A simple tic tac toe game. """

from board import Board
from player import HumanPlayer, ComputerPlayer

import sys


class Game:
    """
    A tic tac toe game.

    Attributes:
        score (dict of Player: int): the score.
        players (list of Player): the players.
    """
    def __init__(self, player1, player2):
        """
        Set attributes.

        Args:
            player1 (Player): one of the players.
            player2 (Player): one of the players.
        """
        self.players = [player2, player1]
        self.score = {player1: 0, player2: 0}

    def play_round(self):
        """
        Play a round of tic tac toe:
        * create a board
        * assign a letter to each player
        * determine the order of play
        * take turns until the game is over
        * update score
        * if we had at least one human player:
            * print the final board and score
        """
        board = Board()
        self.players.reverse()
        self.players[0].symbol = 'x'
        self.players[1].symbol = 'o'
        turn = 0
        while not board.game_over():
            player = self.players[turn % 2]
            player.take_turn(board)
            turn += 1
        winner = board.get_winner()
        if winner is not None:
            self.score[winner] += 1
            result = "{} won!".format(winner.name)
        else:
            result = "The game ended in a draw."
        result += (" {}: {}, {}: {}".format(self.players[0].name,
                                            self.score[self.players[0]],
                                            self.players[1].name,
                                            self.score[self.players[1]]))
        human_player = (isinstance(self.players[0], HumanPlayer)
                        or isinstance(self.players[1], HumanPlayer))
        if human_player:
            print(result)

    def play_game(self):
        """
        Allow the players to play as many rounds of tic tac toe as they want.
        """
        while True:
            self.play_round()
            inpt = input("Want to play another round? (y/n) ")
            if inpt.strip().casefold() not in ['y', 'n']:
                continue
            if inpt.strip().casefold() == 'n':
                break
        print("Thanks for playing!")


def main(player1_name, player2_name):
    """
    Create an instance of Game and let the players play.

    Args:
        player1_name (str): the name of a player. If "computer", create a
            computer player; otherwise create a human player.
        player2_name (str): the name of a player. If "computer", create a
            computer player; otherwise create a human player.
    """
    player1 = (ComputerPlayer('Larry the Computer')
               if player1_name == 'computer'
               else HumanPlayer(player1_name))
    player2 = (ComputerPlayer('Wanda the Computer')
               if player2_name == 'computer'
               else HumanPlayer(player2_name))
    game = Game(player1, player2)
    game.play_game()


if __name__ == '__main__':
    main(sys.argv[1], sys.argv[2])
