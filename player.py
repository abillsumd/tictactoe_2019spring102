""" Implement player classes for a tic tac toe game. """

import random

class Player:
    """
    Abstract tic tac toe player.

    Attributes:
        name (str): the player's name.
        symbol (str): the player's symbol for a particular round.
    """
    def __init__(self, name):
        """ Set a name attribute to the player's name. Set the symbol attribute
            to None."""

        self.name = name
        self.symbol = None

    def take_turn(self, board):
        """
        Select an available square.

        Args:
            board (Board): the tic tac toe board.

        Side effects:
            calls board.player_pick().
        """
        raise NotImplementedError


class HumanPlayer(Player):
    """
    Human tic tac toe player.
    """
    def take_turn(self, board):
        """
        Print the board.
        Prompt the user to select an available square.
        Ensure the user's selection is valid.
        Claim that square for the player.

        Args:
            board (Board): the tic tac toe board.

        Side effects:
            calls board.player_pick().
        """
        while True:
            print(str(board))
            print("You are playing as {}.".format(self.symbol))
            inpt = input("Select an available square: ")
            try:
                square = int(inpt)
                assert square in board.get_free_squares()
            except (ValueError, AssertionError):
                print("Invalid input")
                continue
            else:
                break
        board.player_pick(square, self)


class ComputerPlayer(Player):
    """
    Computer tic tac toe player.
    """
    def take_turn(self, board):
        """
        Select an available square at random.

        Args:
            board (Board): the tic tac toe board.

        Side effects:
            calls board.player_pick().
        """
        if None not in board.game:
            return None

        while True:
            x = random.randint(0,8)
            if board.game[x] == None:
                break

        board.player_pick(x, self)
